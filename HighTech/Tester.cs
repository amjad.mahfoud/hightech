﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HighTech
{
    public class Tester
    {
        private String serial;
        private String name;
        private Generator gen;

        public String Serial
        {
            get { return serial; }
            set 
            {
                this.serial = value;
            }
        }

        public String Name
        {
            get { return name; }
            set 
            {
                this.name = value;
            }
        }

        public Tester(String serial, String name)
        {
            this.serial = serial;
            this.name = name;
        }

        public bool test() 
        {
            gen = new Generator(name);
            if (serial.Equals(gen.Serial)) 
            {
                return true;
            }
            return false;
        }
    }
}
