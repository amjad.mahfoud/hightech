﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HighTech
{
    class Generator
    {
        private string letters = "50kwFn2CJqLIVXc9zrdv86ZHT1ipmUEojGYWsghPeyuQfKtN4AlBO73aSbRDMx";

        public String Letters
        {
            get { return letters;}
            set { this.letters = value;}

        }
        private String name;
        private String serial;

        public String Serial
        {
            get { return serial; }
            set
            {
                this.serial = value;
            }
        }

        public String Name
        {
            get { return name; }
            set
            {
                this.name = value;
            }
        }

        public Generator(String name)
        {
            chickName(name);
            Name = name;
            generateSerial();
        }

        private void generateSerial()
        {
            String[] nameSets = generateNameSets();
            String generatedSerial = "";
            for (int i = 0; i < nameSets.Length; i++)
            {
                generatedSerial += generateSerialSet(nameSets[i], i);
                if (i < 4)
                {
                    generatedSerial += "-";
                }
            }
            Serial = generatedSerial;
        }
        private String[] generateNameSets()
        {
            string[] ret = new string[5];
            for (int i = 0; i < 5; i++)
            {
                String set = "";
                char[] nameArr = name.ToCharArray();
                for (int j = i; j < 2 * name.Length; j++)
                {
                    // set += name.CharAt(j % name.Length);
                    set += nameArr[j % name.Length];
                }
                String t = "";
                char[] setArr = set.ToCharArray();
                for (int k = 0; k < 5; k++)
                {
                    //t += set.charAt(k);
                    t += set[k];
                }
                ret[i] = t;
            }
            return ret;
        }
        private String generateSerialSet(String setGenerator, int setNumber)
        {
            String generatedSet = "";
            char first = setGenerator.ToCharArray()[0];
            char last = setGenerator.ToCharArray()[setGenerator.Length - 1];
            for (int i = 0; i < setGenerator.Length; i++)
            {
                char c = setGenerator.ToCharArray()[i];
                int v = letters.IndexOf(c);
                v += Math.Abs(letters.IndexOf(first) - letters.IndexOf(last));
                v += i * ((letters.IndexOf(first)) + setNumber);
                v = v % letters.Length;
                generatedSet += letters.ToCharArray()[v];
            }
            return generatedSet;
        }
        
        private void chickName(String name)
        {
            if (name.Length < 6)
            {
                // || name.length() > 9
                throw new Exception(
                    "Invalid user name, name length should be greater than 6 letters");
            }
        }        
    }
}
