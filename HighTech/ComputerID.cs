﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace HighTech
{
    public class ComputerID
    {

        public ComputerID()
        {
            this.setSysID();
        }

        private void setSysID()
        {
            this.sysId = this.getMotherBoardID().ToString() + this.getCpuID().ToString();
        }

        private string sysId = "";
        public string SysID
        {
            get { return this.sysId; }
            set
            {
                this.sysId = this.getMotherBoardID().ToString() + this.getCpuID().ToString();
            }
        }
        public string getMotherBoardID()
        {
            string serial = "";
            try
            {
                ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
                ManagementObjectCollection moc = mos.Get();                
                foreach (ManagementObject mo in moc)
                {
                    serial = (string)mo["SerialNumber"];
                }
                return serial;
            }
            catch (Exception) 
            { 
                return serial; 
            }
        }

        public string getCpuID()
        {
            string serial = "";
            try
            {
                ManagementObjectSearcher mbs = new ManagementObjectSearcher("Select * From Win32_processor");
                ManagementObjectCollection mbsList = mbs.Get();

                foreach (ManagementObject mo in mbsList)
                {
                    serial = mo["ProcessorID"].ToString();
                }
            }
            catch (Exception)
            {
                return serial;
            }
            return serial;
        }
    }
}
